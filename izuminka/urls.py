from django.conf.urls import include, url, patterns
import settings
# from page.views import homepage

# urlpatterns = [
#     url(r'^$', homepage, name='homepage'),
# ]

# from django.conf.urls import patterns, include, url
# from django.conf import settings

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns('',
    url(r'^$', 'page.views.homepage', name='homepage'),
    url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}),
)
